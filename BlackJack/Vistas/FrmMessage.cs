﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack.Vistas
{
    public partial class FrmMessage : MetroFramework.Forms.MetroForm
    {
        public string mensaje;
        public string titulo;

        public FrmMessage(string titulo, string mensaje)
        {
            InitializeComponent();
            this.titulo = titulo;
            this.mensaje = mensaje;
            Cargar();
        }

        private void Cargar()
        {
            this.Text = this.titulo;
            txtMessage.Text = this.mensaje;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
