﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack.Vistas
{
    public partial class FrmPerfil : MetroFramework.Forms.MetroForm
    {
        private Modelos.Usuario user;
        List<string> imagenes = new List<string>();

        public FrmPerfil(Modelos.Usuario user)
        {
            InitializeComponent();
            this.user = user;
            Portada();
            CargarDatos();
        }

        private void Portada()
        {
            Random random = new Random();
            int randomImage = random.Next(0, 3);

            imagenes.Add("http://factsongambling.com/wp-content/uploads/2015/01/black-jack-image.jpg");
            imagenes.Add("http://3.bp.blogspot.com/-Rcc-CHWwjJU/UvPKulSKRWI/AAAAAAAAURY/gHRE4B3w_nM/s1600/Los+t%C3%A9rminos+del+Black+Jack.png");
            imagenes.Add("http://casino-navegador.com/es1/uploaded/Juego-de-blackjack-reglamentos-tabla-trucos-4yy3irh1.png");
            imagenes.Add("https://www.gentingcasino.com/images/uploads/games/blackjack.jpg");

            pbPortada.Image = imagen(imagenes.ElementAt(randomImage));
        }

        private void CargarDatos()
        {
            pbFoto.Image = imagen(user.image);
            lblName.Text = user.Name;
            lblEmail2.Text = user.Email;
            lblDinero.Text = Convert.ToString(user.dinero);
            lblFecha.Text = user.Date.Day+"/"+user.Date.Month+"/"+ user.Date.Year;
            lblNivel2.Text = Convert.ToString(user.nivel);
            lblRacha.Text = "";
        }

        private Image imagen(string path)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(path);
            MemoryStream ms = new MemoryStream(bytes);
            Image img = Image.FromStream(ms);
            return img;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
