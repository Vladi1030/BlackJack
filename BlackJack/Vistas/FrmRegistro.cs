﻿using System;using System.Drawing;

namespace BlackJack.Vistas
{
    public partial class FrmRegistro : MetroFramework.Forms.MetroForm
    {
        public FrmRegistro()
        {
            InitializeComponent();
        }

        private void mbtnGuardar_Click(object sender, EventArgs e)
        {
            NuevoUsuario();
        }

        private void NuevoUsuario()
        {
            if (txtPassword.Text.Trim().Equals(txtVerPassword.Text.Trim()))
            {
                Modelos.Usuario user = new Modelos.Usuario();
                user.Name = txtNombre.Text.Trim() + " " + txtApellidos.Text.Trim();
                user.Email = txtCorreo.Text.Trim();
                user.Date = dtpDate.Value;
                user.Password = txtPassword.Text.Trim();
                user.image = "https://image.flaticon.com/icons/svg/34/34296.svg";
                user.ganadas = 0;
                user.perdidas = 0;
                user.nivel = 0;
                user.dinero = 0;
                user.is_google = false;

                if (!user.Name.Equals("") && !user.Email.Equals("")) {
                    try
                    {
                        Controladores.Usuario u = new Controladores.Usuario(user);
                        u.Insert();
                        lblMessage.ForeColor = Color.Green;
                        lblMessage.Text = "Usuario agregado, inicie sesión para jugar";
                    }
                    catch (Exception)
                    {
                        lblMessage.ForeColor = Color.Red;
                        lblMessage.Text = "Falló el intento, revise los datos";
                    }
                }
                else
                {
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Text = "Todos los datos son necesarios";
                }
                return;
            }
            
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = "Verifique las contraseñas";
        }

        private void FrmRegistro_Load(object sender, EventArgs e)
        {

        }
    }
}
