﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using BlackJack.Utils;

namespace BlackJack.Vistas
{
    public partial class FrmMain : MetroFramework.Forms.MetroForm
    {
        private Modelos.Usuario user;
        private string password; 

        public FrmMain()
        {
            InitializeComponent();
            cargar();
        }

        public FrmMain(Modelos.Usuario user)
        {
            InitializeComponent();
            this.user = user;
            cargar();
        }

        private void ActualizarUsuario()
        {
            Controladores.Usuario userNew = new Controladores.Usuario();
            DataTable dt = userNew.Verificar(this.user.Email, this.user.Password);
            Modelos.Usuario u = new Modelos.Usuario();
            u.Name = Convert.ToString(dt.Rows[0]["nombre"]);
            u.Id = Convert.ToInt32(dt.Rows[0]["id"]);
            u.Date = Convert.ToDateTime(dt.Rows[0]["fecha"]);
            u.Email = Convert.ToString(dt.Rows[0]["correo"]);
            u.Password = Convert.ToString(dt.Rows[0]["contrasena"]);
            u.image = Convert.ToString(dt.Rows[0]["image"]);
            u.dinero = Convert.ToDouble(dt.Rows[0]["dinero"]);
            u.nivel = Convert.ToInt32(dt.Rows[0]["nivel"]);
            u.ganadas = Convert.ToInt32(dt.Rows[0]["ganadas"]);
            u.perdidas = Convert.ToInt32(dt.Rows[0]["perdidas"]);
            u.is_google = Convert.ToBoolean(dt.Rows[0]["is_google"]);
            this.user = u;
            cargar();
        }

        private void cargar()
        {
            Nivel();
            lblNombre.Text = user.Name;
            lblDinero.Text = "Dinero actual: $"+user.dinero.ToString();
            lblGanadas.Text = "Partidas ganadas: "+user.ganadas.ToString();
            lblPerdidas.Text = "Partidas perdidas: " + user.perdidas.ToString();
            lblLevel.Text = "Nivel: " + user.nivel.ToString();

            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            gp.AddEllipse(0, 0, pbFoto.Width - 3, pbFoto.Height - 3);
            Region rg = new Region(gp);
            pbFoto.Region = rg;

            string info = "Cuenta de " + user.Name;
            info += "\n" + user.Email;
            info += "\nPresione para cerrar sesión";
            ToolTip toolTip1 = new ToolTip();
            toolTip1.SetToolTip(this.pbFoto, info);
            toolTip1.SetToolTip(this.pbNivel, "Presione para ver los niveles");

            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(this.user.image);
            MemoryStream ms = new MemoryStream(bytes);
            Image img = Image.FromStream(ms);
            pbFoto.Image = img;
        }

        private void Nivel()
        {
            string file = "";
            if (user.nivel<200)
            {
                file = "..\\images\\principiante.jpg";
                lbllblLiga.Text = "Liga: Principiante";
            }else if (user.nivel < 350)
            {
                file = "..\\images\\novato.jpg";
                lbllblLiga.Text = "Liga: Novato";
            } else if (user.nivel < 500)
            {
                file = "..\\images\\inexperto.jpg";
                lbllblLiga.Text = "Liga: Inexperto";
            } else if (user.nivel > 500)
            {
                file = "..\\images\\pro.jpg";
                lbllblLiga.Text = "Liga: Pro";
            }

           //pbNivel.Image = Image.FromFile(file);
        }

        private void label1_MouseClick(object sender, MouseEventArgs e)
        {
            FrmInfo oInfo = new FrmInfo();
            oInfo.Show();
        }

        private void pbFoto_MouseClick(object sender, MouseEventArgs e)
        {
            FrmInicial oInicial = new FrmInicial();
            oInicial.Show();
            this.Close();           
        }

        private void pbNivel_MouseClick(object sender, MouseEventArgs e)
        {
            FrmLigas oLigas = new FrmLigas();
            oLigas.Show();
        }

        private void btnNueva_Click(object sender, EventArgs e)
        {
            FrmJuego oJuego = new FrmJuego(this.user);
            oJuego.Show();
        }

        private void label6_MouseClick(object sender, MouseEventArgs e)
        {
            ActualizarUsuario();
            cargar();
        }

        private void label2_MouseClick(object sender, MouseEventArgs e)
        {
            FrmPerfil oPerfil = new FrmPerfil(this.user);
            oPerfil.Show();
        }

        private void label3_MouseClick(object sender, MouseEventArgs e)
        {
            password = UserData.Contrasena(this.user.Id);
            ActualizarUsuario();
            if (!this.user.is_google)
            {
                FrmContrasena oContrasena = new FrmContrasena(this.user);
                oContrasena.ShowDialog();
            }
            else
            {
                FrmMessage oMessage = new FrmMessage("Nota", "Los usuarios logueados con Google+\n" +
                     "no tienen acceso a esta acción");
                oMessage.Show();
            }
        }

        private void label4_MouseClick(object sender, MouseEventArgs e)
        {
            FrmPosiciones oPos = new FrmPosiciones(this.user);
            oPos.Show();
        }
    }
}