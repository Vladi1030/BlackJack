﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackJack.Utils;

namespace BlackJack.Vistas
{
    public partial class FrmPosiciones : MetroFramework.Forms.MetroForm
    {
        private Modelos.Usuario user;

        public FrmPosiciones(Modelos.Usuario user)
        {
            InitializeComponent();
            this.user = user;
            CargarPosiciones();
        }

        private void CargarPosiciones()
        {
            int[] niveles = nivel();
            DataTable dt = UserData.Posiciones(niveles[0], niveles[1]);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtvPosiciones.DefaultCellStyle.SelectionBackColor = Color.White;
                dtvPosiciones.DefaultCellStyle.SelectionForeColor = Color.Black;
                Object []data = new Object[3];
                data[0] = i+1;
                data[2] = dt.Rows[i]["nombre"];
                data[1] = dt.Rows[i]["nivel"];
                dtvPosiciones.Rows.Add(data);

                if (data[2].Equals(user.Name))
                {
                    dtvPosiciones.Rows[i].Cells[0].Style.BackColor = Color.AliceBlue;
                    dtvPosiciones.Rows[i].Cells[1].Style.BackColor = Color.AliceBlue;
                    dtvPosiciones.Rows[i].Cells[2].Style.BackColor = Color.AliceBlue;
                }
            }
        }

        private int[] nivel()
        {
            int[] nivel = new int[2];
            if (user.nivel < 200)
            {
                nivel[0] = 0;
                nivel[1] = 200;
                return nivel;
            }
            else if (user.nivel < 350)
            {
                nivel[0] = 199;
                nivel[1] = 350;
                return nivel;
            }
            else if (user.nivel < 500)
            {
                nivel[0] = 349;
                nivel[1] = 500;
                return nivel;
            }
            else if (user.nivel > 500)
            {
                nivel[0] = 499;
                nivel[1] = 99999;
                return nivel;
            }
            return nivel;
        }

        private void btnListo_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
