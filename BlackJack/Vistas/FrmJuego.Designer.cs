﻿namespace BlackJack.Vistas
{
    partial class FrmJuego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJuego));
            this.pbFondo = new System.Windows.Forms.PictureBox();
            this.pbCasa = new System.Windows.Forms.PictureBox();
            this.pbCasa5 = new System.Windows.Forms.PictureBox();
            this.pbCasa3 = new System.Windows.Forms.PictureBox();
            this.pbCasa1 = new System.Windows.Forms.PictureBox();
            this.pbCasa2 = new System.Windows.Forms.PictureBox();
            this.pbCasa4 = new System.Windows.Forms.PictureBox();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnPedir = new System.Windows.Forms.Button();
            this.btnDoblar = new System.Windows.Forms.Button();
            this.btnPasar = new System.Windows.Forms.Button();
            this.gbJuego = new System.Windows.Forms.GroupBox();
            this.pbUser = new System.Windows.Forms.PictureBox();
            this.lblCuCasa = new System.Windows.Forms.Label();
            this.lblCuUser = new System.Windows.Forms.Label();
            this.pbJug8 = new System.Windows.Forms.PictureBox();
            this.pbJug6 = new System.Windows.Forms.PictureBox();
            this.pbJug4 = new System.Windows.Forms.PictureBox();
            this.pbJug2 = new System.Windows.Forms.PictureBox();
            this.pbJug1 = new System.Windows.Forms.PictureBox();
            this.pbJug3 = new System.Windows.Forms.PictureBox();
            this.pbJug5 = new System.Windows.Forms.PictureBox();
            this.pbJug7 = new System.Windows.Forms.PictureBox();
            this.pbJug9 = new System.Windows.Forms.PictureBox();
            this.pbJug11 = new System.Windows.Forms.PictureBox();
            this.pbJug10 = new System.Windows.Forms.PictureBox();
            this.gbAdicional = new System.Windows.Forms.GroupBox();
            this.lblNameCasa = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa4)).BeginInit();
            this.gbJuego.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug10)).BeginInit();
            this.gbAdicional.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbFondo
            // 
            this.pbFondo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbFondo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbFondo.Image = ((System.Drawing.Image)(resources.GetObject("pbFondo.Image")));
            this.pbFondo.Location = new System.Drawing.Point(0, 30);
            this.pbFondo.Name = "pbFondo";
            this.pbFondo.Size = new System.Drawing.Size(904, 585);
            this.pbFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFondo.TabIndex = 0;
            this.pbFondo.TabStop = false;
            // 
            // pbCasa
            // 
            this.pbCasa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCasa.BackColor = System.Drawing.Color.Transparent;
            this.pbCasa.Location = new System.Drawing.Point(822, 75);
            this.pbCasa.Name = "pbCasa";
            this.pbCasa.Size = new System.Drawing.Size(59, 57);
            this.pbCasa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCasa.TabIndex = 1;
            this.pbCasa.TabStop = false;
            // 
            // pbCasa5
            // 
            this.pbCasa5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCasa5.BackColor = System.Drawing.Color.Transparent;
            this.pbCasa5.Location = new System.Drawing.Point(355, 139);
            this.pbCasa5.Name = "pbCasa5";
            this.pbCasa5.Size = new System.Drawing.Size(63, 87);
            this.pbCasa5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCasa5.TabIndex = 6;
            this.pbCasa5.TabStop = false;
            // 
            // pbCasa3
            // 
            this.pbCasa3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCasa3.BackColor = System.Drawing.Color.Transparent;
            this.pbCasa3.Location = new System.Drawing.Point(390, 150);
            this.pbCasa3.Name = "pbCasa3";
            this.pbCasa3.Size = new System.Drawing.Size(63, 87);
            this.pbCasa3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCasa3.TabIndex = 7;
            this.pbCasa3.TabStop = false;
            // 
            // pbCasa1
            // 
            this.pbCasa1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCasa1.BackColor = System.Drawing.Color.Transparent;
            this.pbCasa1.Location = new System.Drawing.Point(424, 160);
            this.pbCasa1.Name = "pbCasa1";
            this.pbCasa1.Size = new System.Drawing.Size(63, 87);
            this.pbCasa1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCasa1.TabIndex = 9;
            this.pbCasa1.TabStop = false;
            // 
            // pbCasa2
            // 
            this.pbCasa2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCasa2.BackColor = System.Drawing.Color.Transparent;
            this.pbCasa2.Location = new System.Drawing.Point(459, 150);
            this.pbCasa2.Name = "pbCasa2";
            this.pbCasa2.Size = new System.Drawing.Size(63, 87);
            this.pbCasa2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCasa2.TabIndex = 10;
            this.pbCasa2.TabStop = false;
            // 
            // pbCasa4
            // 
            this.pbCasa4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCasa4.BackColor = System.Drawing.Color.Transparent;
            this.pbCasa4.Location = new System.Drawing.Point(493, 139);
            this.pbCasa4.Name = "pbCasa4";
            this.pbCasa4.Size = new System.Drawing.Size(63, 87);
            this.pbCasa4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCasa4.TabIndex = 11;
            this.pbCasa4.TabStop = false;
            // 
            // btnIniciar
            // 
            this.btnIniciar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIniciar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnIniciar.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIniciar.ForeColor = System.Drawing.Color.White;
            this.btnIniciar.Location = new System.Drawing.Point(11, 63);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(214, 30);
            this.btnIniciar.TabIndex = 0;
            this.btnIniciar.Text = "Iniciar partida";
            this.btnIniciar.UseVisualStyleBackColor = false;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnPedir
            // 
            this.btnPedir.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnPedir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnPedir.Font = new System.Drawing.Font("Maiandra GD", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPedir.Location = new System.Drawing.Point(5, 24);
            this.btnPedir.Name = "btnPedir";
            this.btnPedir.Size = new System.Drawing.Size(76, 30);
            this.btnPedir.TabIndex = 13;
            this.btnPedir.Text = "Pedir";
            this.btnPedir.UseVisualStyleBackColor = false;
            this.btnPedir.Click += new System.EventHandler(this.btnPedir_Click);
            // 
            // btnDoblar
            // 
            this.btnDoblar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDoblar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnDoblar.Font = new System.Drawing.Font("Maiandra GD", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoblar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDoblar.Location = new System.Drawing.Point(87, 24);
            this.btnDoblar.Name = "btnDoblar";
            this.btnDoblar.Size = new System.Drawing.Size(76, 30);
            this.btnDoblar.TabIndex = 14;
            this.btnDoblar.Text = "Doblar";
            this.btnDoblar.UseVisualStyleBackColor = false;
            // 
            // btnPasar
            // 
            this.btnPasar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnPasar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnPasar.Font = new System.Drawing.Font("Maiandra GD", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPasar.Location = new System.Drawing.Point(169, 24);
            this.btnPasar.Name = "btnPasar";
            this.btnPasar.Size = new System.Drawing.Size(76, 30);
            this.btnPasar.TabIndex = 15;
            this.btnPasar.Text = "Pasar";
            this.btnPasar.UseVisualStyleBackColor = false;
            // 
            // gbJuego
            // 
            this.gbJuego.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gbJuego.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.gbJuego.Controls.Add(this.btnDoblar);
            this.gbJuego.Controls.Add(this.btnIniciar);
            this.gbJuego.Controls.Add(this.btnPedir);
            this.gbJuego.Controls.Add(this.btnPasar);
            this.gbJuego.Font = new System.Drawing.Font("Maiandra GD", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbJuego.ForeColor = System.Drawing.Color.White;
            this.gbJuego.Location = new System.Drawing.Point(631, 499);
            this.gbJuego.Name = "gbJuego";
            this.gbJuego.Size = new System.Drawing.Size(250, 106);
            this.gbJuego.TabIndex = 17;
            this.gbJuego.TabStop = false;
            this.gbJuego.Text = "Jugar";
            // 
            // pbUser
            // 
            this.pbUser.BackColor = System.Drawing.Color.Transparent;
            this.pbUser.Location = new System.Drawing.Point(23, 75);
            this.pbUser.Name = "pbUser";
            this.pbUser.Size = new System.Drawing.Size(59, 57);
            this.pbUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUser.TabIndex = 18;
            this.pbUser.TabStop = false;
            // 
            // lblCuCasa
            // 
            this.lblCuCasa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCuCasa.AutoSize = true;
            this.lblCuCasa.BackColor = System.Drawing.Color.Transparent;
            this.lblCuCasa.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuCasa.ForeColor = System.Drawing.Color.White;
            this.lblCuCasa.Location = new System.Drawing.Point(719, 75);
            this.lblCuCasa.Name = "lblCuCasa";
            this.lblCuCasa.Size = new System.Drawing.Size(87, 19);
            this.lblCuCasa.TabIndex = 19;
            this.lblCuCasa.Text = "Cuenta: 0";
            // 
            // lblCuUser
            // 
            this.lblCuUser.AutoSize = true;
            this.lblCuUser.BackColor = System.Drawing.Color.Transparent;
            this.lblCuUser.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuUser.ForeColor = System.Drawing.Color.White;
            this.lblCuUser.Location = new System.Drawing.Point(88, 75);
            this.lblCuUser.Name = "lblCuUser";
            this.lblCuUser.Size = new System.Drawing.Size(87, 19);
            this.lblCuUser.TabIndex = 20;
            this.lblCuUser.Text = "Cuenta: 0";
            // 
            // pbJug8
            // 
            this.pbJug8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug8.BackColor = System.Drawing.Color.Transparent;
            this.pbJug8.Location = new System.Drawing.Point(514, 377);
            this.pbJug8.Name = "pbJug8";
            this.pbJug8.Size = new System.Drawing.Size(63, 87);
            this.pbJug8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug8.TabIndex = 21;
            this.pbJug8.TabStop = false;
            // 
            // pbJug6
            // 
            this.pbJug6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug6.BackColor = System.Drawing.Color.Transparent;
            this.pbJug6.Location = new System.Drawing.Point(497, 367);
            this.pbJug6.Name = "pbJug6";
            this.pbJug6.Size = new System.Drawing.Size(63, 87);
            this.pbJug6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug6.TabIndex = 22;
            this.pbJug6.TabStop = false;
            // 
            // pbJug4
            // 
            this.pbJug4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug4.BackColor = System.Drawing.Color.Transparent;
            this.pbJug4.Location = new System.Drawing.Point(474, 358);
            this.pbJug4.Name = "pbJug4";
            this.pbJug4.Size = new System.Drawing.Size(63, 87);
            this.pbJug4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug4.TabIndex = 23;
            this.pbJug4.TabStop = false;
            // 
            // pbJug2
            // 
            this.pbJug2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug2.BackColor = System.Drawing.Color.Transparent;
            this.pbJug2.Location = new System.Drawing.Point(454, 348);
            this.pbJug2.Name = "pbJug2";
            this.pbJug2.Size = new System.Drawing.Size(63, 87);
            this.pbJug2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug2.TabIndex = 24;
            this.pbJug2.TabStop = false;
            // 
            // pbJug1
            // 
            this.pbJug1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug1.BackColor = System.Drawing.Color.Transparent;
            this.pbJug1.Location = new System.Drawing.Point(436, 339);
            this.pbJug1.Name = "pbJug1";
            this.pbJug1.Size = new System.Drawing.Size(63, 87);
            this.pbJug1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug1.TabIndex = 25;
            this.pbJug1.TabStop = false;
            // 
            // pbJug3
            // 
            this.pbJug3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug3.BackColor = System.Drawing.Color.Transparent;
            this.pbJug3.Location = new System.Drawing.Point(414, 348);
            this.pbJug3.Name = "pbJug3";
            this.pbJug3.Size = new System.Drawing.Size(63, 87);
            this.pbJug3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug3.TabIndex = 26;
            this.pbJug3.TabStop = false;
            // 
            // pbJug5
            // 
            this.pbJug5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug5.BackColor = System.Drawing.Color.Transparent;
            this.pbJug5.Location = new System.Drawing.Point(394, 358);
            this.pbJug5.Name = "pbJug5";
            this.pbJug5.Size = new System.Drawing.Size(63, 87);
            this.pbJug5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug5.TabIndex = 27;
            this.pbJug5.TabStop = false;
            // 
            // pbJug7
            // 
            this.pbJug7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug7.BackColor = System.Drawing.Color.Transparent;
            this.pbJug7.Location = new System.Drawing.Point(375, 367);
            this.pbJug7.Name = "pbJug7";
            this.pbJug7.Size = new System.Drawing.Size(63, 87);
            this.pbJug7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug7.TabIndex = 28;
            this.pbJug7.TabStop = false;
            // 
            // pbJug9
            // 
            this.pbJug9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbJug9.BackColor = System.Drawing.Color.Transparent;
            this.pbJug9.Location = new System.Drawing.Point(354, 377);
            this.pbJug9.Name = "pbJug9";
            this.pbJug9.Size = new System.Drawing.Size(63, 87);
            this.pbJug9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug9.TabIndex = 29;
            this.pbJug9.TabStop = false;
            // 
            // pbJug11
            // 
            this.pbJug11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.pbJug11.Location = new System.Drawing.Point(80, 21);
            this.pbJug11.Name = "pbJug11";
            this.pbJug11.Size = new System.Drawing.Size(63, 87);
            this.pbJug11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug11.TabIndex = 30;
            this.pbJug11.TabStop = false;
            // 
            // pbJug10
            // 
            this.pbJug10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.pbJug10.Location = new System.Drawing.Point(7, 21);
            this.pbJug10.Name = "pbJug10";
            this.pbJug10.Size = new System.Drawing.Size(63, 87);
            this.pbJug10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbJug10.TabIndex = 31;
            this.pbJug10.TabStop = false;
            // 
            // gbAdicional
            // 
            this.gbAdicional.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbAdicional.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.gbAdicional.Controls.Add(this.pbJug10);
            this.gbAdicional.Controls.Add(this.pbJug11);
            this.gbAdicional.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAdicional.ForeColor = System.Drawing.Color.White;
            this.gbAdicional.Location = new System.Drawing.Point(23, 491);
            this.gbAdicional.Name = "gbAdicional";
            this.gbAdicional.Size = new System.Drawing.Size(154, 114);
            this.gbAdicional.TabIndex = 32;
            this.gbAdicional.TabStop = false;
            this.gbAdicional.Text = "Cartas adicionales";
            // 
            // lblNameCasa
            // 
            this.lblNameCasa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNameCasa.AutoSize = true;
            this.lblNameCasa.BackColor = System.Drawing.Color.Transparent;
            this.lblNameCasa.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameCasa.ForeColor = System.Drawing.Color.White;
            this.lblNameCasa.Location = new System.Drawing.Point(771, 46);
            this.lblNameCasa.Name = "lblNameCasa";
            this.lblNameCasa.Size = new System.Drawing.Size(45, 19);
            this.lblNameCasa.TabIndex = 34;
            this.lblNameCasa.Text = "Casa";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(88, 46);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(56, 19);
            this.lblName.TabIndex = 33;
            this.lblName.Text = "Name";
            // 
            // FrmJuego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 614);
            this.Controls.Add(this.lblNameCasa);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.gbAdicional);
            this.Controls.Add(this.pbJug9);
            this.Controls.Add(this.pbJug7);
            this.Controls.Add(this.pbJug5);
            this.Controls.Add(this.pbJug3);
            this.Controls.Add(this.pbJug1);
            this.Controls.Add(this.pbJug2);
            this.Controls.Add(this.pbJug4);
            this.Controls.Add(this.pbJug6);
            this.Controls.Add(this.pbJug8);
            this.Controls.Add(this.lblCuUser);
            this.Controls.Add(this.lblCuCasa);
            this.Controls.Add(this.pbUser);
            this.Controls.Add(this.gbJuego);
            this.Controls.Add(this.pbCasa4);
            this.Controls.Add(this.pbCasa2);
            this.Controls.Add(this.pbCasa1);
            this.Controls.Add(this.pbCasa3);
            this.Controls.Add(this.pbCasa5);
            this.Controls.Add(this.pbCasa);
            this.Controls.Add(this.pbFondo);
            this.Name = "FrmJuego";
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCasa4)).EndInit();
            this.gbJuego.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJug10)).EndInit();
            this.gbAdicional.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbFondo;
        private System.Windows.Forms.PictureBox pbCasa;
        private System.Windows.Forms.PictureBox pbCasa5;
        private System.Windows.Forms.PictureBox pbCasa3;
        private System.Windows.Forms.PictureBox pbCasa1;
        private System.Windows.Forms.PictureBox pbCasa2;
        private System.Windows.Forms.PictureBox pbCasa4;
        private System.Windows.Forms.Button btnPasar;
        private System.Windows.Forms.Button btnDoblar;
        private System.Windows.Forms.Button btnPedir;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.GroupBox gbJuego;
        private System.Windows.Forms.PictureBox pbUser;
        private System.Windows.Forms.Label lblCuCasa;
        private System.Windows.Forms.Label lblCuUser;
        private System.Windows.Forms.PictureBox pbJug8;
        private System.Windows.Forms.PictureBox pbJug6;
        private System.Windows.Forms.PictureBox pbJug4;
        private System.Windows.Forms.PictureBox pbJug2;
        private System.Windows.Forms.PictureBox pbJug1;
        private System.Windows.Forms.PictureBox pbJug3;
        private System.Windows.Forms.PictureBox pbJug5;
        private System.Windows.Forms.PictureBox pbJug7;
        private System.Windows.Forms.PictureBox pbJug9;
        private System.Windows.Forms.PictureBox pbJug11;
        private System.Windows.Forms.PictureBox pbJug10;
        private System.Windows.Forms.GroupBox gbAdicional;
        private System.Windows.Forms.Label lblNameCasa;
        private System.Windows.Forms.Label lblName;
    }
}