﻿using BlackJack.Modelos;
using BlackJack.Services;
using BlackJack.Utils;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack.Vistas
{
    public partial class FrmJuego : MetroFramework.Forms.MetroForm
    {
        private Usuario user;
        private static HttpClient client = new HttpClient();
        private Deck deck;
        private Card first;
        private Logica log;
        private Card card;
        private int cuentaJug;
        private int cuentaCasa;
        private CardsResponse cr;
        private int value;
        private string apuesta;

        public FrmJuego(Modelos.Usuario user)
        {
            InitializeComponent();
            this.user = user;
            this.log = new Logica();
            card = null;
            deck = null;
            cr = null;
            cargar();
            cargarImagen();
        }

        private async Task Repartir()
        {
            for(int i = 0; i < 2; i++)
            {
                PictureBox pb = null;
                cuentaCasa++;
                this.cr = await getCarta();
                pb = Casa(cuentaCasa);
                if(cuentaCasa!=1)
                {
                    log.aumentarCasa(card.value);
                    pb.Image = ConvertToImage("https://www.enjoy.cl/san-andres/wp-content/uploads/sites/41/2014/12/naipes-casino-enjoy-900x600.jpg");
                }
                else
                {
                    first = card;
                    //pb.Image = Image.FromFile("C:\\Users\\vmata\\Downloads\\Tefa\\BlackJack\\BlackJack\\images\\backcard.jpg");
                    pb.Image = ConvertToImage("https://www.enjoy.cl/san-andres/wp-content/uploads/sites/41/2014/12/naipes-casino-enjoy-900x600.jpg");
                }
                lblCuCasa.Text = "Cuenta: " + log.cuentaCasa;

                cuentaJug++;
                pb = Jug(cuentaJug);
                this.cr = await getCarta();
                if (!card.value.Equals("ACE")) { 
                    log.aumentarJug(card.value);
                }
                else
                {
                    validarValor(card.value);
                    log.aumentarJug(this.value.ToString());
                }
                lblCuUser.Text = "Cuenta: " + log.cuentaJug;
                pb.Image = ConvertToImage(card.image);
            }
        }

        private void clean()
        {
            log = new Logica();
            cuentaJug = 0;
            cuentaCasa = 0;
            lblCuCasa.Text = "Cuenta: 0";
            lblCuUser.Text = "Cuenta: 0";
            pbCasa1.Image = null;
            pbCasa2.Image = null;
            pbCasa3.Image = null;
            pbCasa4.Image = null;
            pbCasa5.Image = null;
            pbJug1.Image = null;
            pbJug2.Image = null;
            pbJug3.Image = null;
            pbJug4.Image = null;
            pbJug5.Image = null;
            pbJug6.Image = null;
            pbJug7.Image = null;
            pbJug8.Image = null;
            pbJug9.Image = null;
            pbJug10.Image = null;
            pbJug11.Image = null;
            adicional(false);
        }

        private void validarValor(string val)
        {
            if (val.Equals("ACE"))
            {
                string titulo = "AS bajo la manga";
                string mensaje = "Puedes escoger entre 1 o 11\ncomo valor por obtener el AS";
                FrmValor oValor = new FrmValor(titulo, mensaje, "ACE");
                oValor.ShowDialog();
                this.value = oValor.valor;
            }
        }

        private async Task<Deck> GetDeckAsync()
        {
            Deck deck = null;

            HttpResponseMessage response = await client.GetAsync(Servicios.url);
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                deck = JsonConvert.DeserializeObject<Deck>(data);
            }
            return deck;
        }

        private async Task<CardsResponse> getCarta()
        {
            cr = null;
            string url = Servicios.urlRetrieveCards.Replace("{id}", this.deck.deck_id);
            HttpResponseMessage response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                this.cr = JsonConvert.DeserializeObject<CardsResponse>(data);
                this.card = cr.cards[0];
            }

            return cr;
        }

        private void cargarImagen()
        {
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            gp.AddEllipse(0, 0, pbCasa.Width - 3, pbCasa.Height - 3);
            Region rg = new Region(gp);
            pbUser.Region = rg;
            pbCasa.Region = rg;
            pbCasa.Image = ConvertToImage("https://www.enjoy.cl/san-andres/wp-content/uploads/sites/41/2014/12/naipes-casino-enjoy-900x600.jpg");
            pbUser.Image = ConvertToImage(this.user.image);
        }

        private Image ConvertToImage(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(url);
            MemoryStream ms = new MemoryStream(bytes);
            Image img = Image.FromStream(ms);
            return img;
        }

        private void cargar()
        {
            lblName.Parent = pbFondo;
            lblNameCasa.Parent = pbFondo;
            lblCuCasa.Parent = pbFondo;
            lblCuUser.Parent = pbFondo;
            pbCasa1.Parent = pbFondo;
            pbCasa2.Parent = pbFondo;
            pbCasa3.Parent = pbFondo;
            pbCasa4.Parent = pbFondo;
            pbCasa5.Parent = pbFondo;
            pbJug1.Parent = pbFondo;
            pbJug2.Parent = pbFondo;
            pbJug3.Parent = pbFondo;
            pbJug4.Parent = pbFondo;
            pbJug5.Parent = pbFondo;
            pbJug6.Parent = pbFondo;
            pbJug7.Parent = pbFondo;
            pbJug8.Parent = pbFondo;
            pbJug9.Parent = pbFondo;
            lblName.Text = this.user.Name;
            adicional(false);
            btnDoblar.Enabled = false;
            btnPasar.Enabled = false;
            btnPedir.Enabled = false;
        }

        private void adicional(bool per)
        {
            gbAdicional.Visible = per;
            pbJug10.Visible = per;
            pbJug11.Visible = per;
        }

        private async void Iniciar()
        {
            this.deck = await GetDeckAsync();
            bloqBottons(false);
            await Repartir();
            bloqBottons(true);
        }

        private void bloqBottons(bool per)
        {
            btnDoblar.Enabled = per;
            btnPasar.Enabled = per;
            btnPedir.Enabled = per;
        }

        private async Task pedir()
        {
            if (log.cuentaJug < 21) { 
                PictureBox pb = null;
                cuentaJug++;
                pb = Jug(cuentaJug);
                this.cr = await getCarta();
                if (!card.value.Equals("ACE"))
                {
                    log.aumentarJug(card.value);
                }
                else
                {
                    validarValor(card.value);
                    log.aumentarJug(this.value.ToString());
                }
                lblCuUser.Text = "Cuenta: " + log.cuentaJug;
                pb.Image = ConvertToImage(card.image);
                pb.Visible = true;
            }
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            btnIniciar.Text = "Partida nueva";
            clean();
            Iniciar();
        }

        private PictureBox Jug(int count)
        {
            if (count == 1)
            {
                return this.pbJug1;
            }
            else if (count == 2)
            {
                return this.pbJug2;
            }
            else if (count == 3)
            {
                return this.pbJug3;
            }
            else if (count == 4)
            {
                return this.pbJug4;
            }
            else if (count == 5)
            {
                return this.pbJug5;
            }
            else if (count == 6)
            {
                return this.pbJug6;
            }
            else if (count == 7)
            {
                return this.pbJug7;
            }
            else if (count == 8)
            {
                return this.pbJug8;
            }
            else if (count == 9)
            {
                return this.pbJug9;
            }
            else if (count == 10)
            {
                adicional(true);
                return this.pbJug10;
            }
            else if (count == 11)
            {
                return this.pbJug11;
            }
            return null;
        }

        private PictureBox Casa(int count)
        {
            if(count == 1)
            {
                return this.pbCasa1;
            }else if (count == 2)
            {
                return this.pbCasa2;
            }
            else if (count == 3)
            {
                return this.pbCasa3;
            }
            else if (count == 4)
            {
                return this.pbCasa4;
            }
            else if (count == 5)
            {
                return this.pbCasa4;
            }
            return null;
        }

        private void validarFin()
        {
            if ((log.cuentaJug > 21))
            {
                final(false);
                clean();
                MessageBox.Show("Perdió");
                Iniciar();
            }
        }

        private void final(bool per)
        {
            Controladores.Usuario u = new Controladores.Usuario(this.user);
            u.Update(per, 1);
        }

        private async void btnPedir_Click(object sender, EventArgs e)
        {
            bloqBottons(false);
            await pedir();
            bloqBottons(true);
            validarFin();
        }
    }
}
