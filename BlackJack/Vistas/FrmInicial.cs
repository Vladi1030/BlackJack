﻿using System;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Plus.v1;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using Google.Apis.Plus.v1.Data;
using System.Data;
using DotNetOpenAuth.OAuth2;

namespace BlackJack.Vistas
{
    public partial class FrmInicial : MetroFramework.Forms.MetroForm
    {
        private FrmMain oMain;

        public FrmInicial()
        {
            InitializeComponent();
            Cargar();
        }

        private void Cargar()
        {
            chkContrasena.Checked = true;
            chkRemember.Checked = true;
        }

        private void chkContrasena_CheckedChanged(object sender, EventArgs e)
        {
            if (!(chkContrasena.Checked))
            {
                txtPassword.PasswordChar = '*';
            }
            else
            {
                txtPassword.PasswordChar = '\0';
            }
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            FrmRegistro oRegistro = new FrmRegistro();
            oRegistro.Show();
        }

        private void txtPassword_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtPassword.Text = "";
        }

        private void txtLogin_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtLogin.Text = "";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            NuevoGoogleUser();
        }

        private async void NuevoGoogleUser()
        {
            string[] scopes = new string[] { PlusService.Scope.PlusLogin,
            PlusService.Scope.UserinfoEmail, PlusService.Scope.UserinfoProfile };
            

            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets { ClientId = "1005852313512-f8nqohopiqj03h2evhvct0kgm79nmfm4.apps.googleusercontent.com", ClientSecret = "avpO91f2wooDlNde9ROVBP9R" },
                scopes , Environment.UserName, CancellationToken.None, new FileDataStore("client_id.json")).Result;


            PlusService service = new PlusService(new BaseClientService.Initializer() { HttpClientInitializer = credential, ApplicationName = "BlackJack", });
            PeopleResource.GetRequest personRequest = service.People.Get("me");
            Person me = personRequest.Execute();

            Modelos.Usuario user = new Modelos.Usuario();
            user.Name = me.Name.GivenName + " " + me.Name.FamilyName;
            user.Email = me.Emails[0].Value;
            user.Date = Convert.ToDateTime(me.Birthday);
            user.Password = "";
            if (me.Image.Url.Equals(""))
            {
                user.image = "https://image.flaticon.com/icons/svg/34/34296.svg";
            }
            else
            {
                user.image = me.Image.Url;
            }
            user.ganadas = 0;
            user.perdidas = 0;
            user.nivel = 0;
            user.dinero = 0;
            user.is_google = true;

            DataTable dt = Verificar(user.Email, "");

            if (!(chkRemember.Checked))
            {
                await credential.RevokeTokenAsync(CancellationToken.None);
            }

            if (dt.Rows.Count>0) {
                
                Iniciar(dt);
            }
            else
            {
                Controladores.Usuario u = new Controladores.Usuario(user);
                u.Insert();
                dt = Verificar(user.Email, "");
                if(dt.Rows.Count > 0)
                {
                    Iniciar(dt);
                }
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            DataTable dt = Verificar(txtLogin.Text.Trim(), txtPassword.Text.Trim());

            if (dt.Rows.Count > 0)
            {
                Iniciar(dt);
            }
            else
            {
                string titulo = "Datos incorrectos";
                string mensaje = "La contraseña o el correo son incorrectos, " +
                    "por favor\nverifique que estén bien y vuelva a intentarlo.\n\nSi" +
                    "no tiene una cuenta, registrese gratis.";
                FrmMessage oMessage = new FrmMessage(titulo, mensaje);
                oMessage.ShowDialog();
            }
        }

        private void Iniciar(DataTable dt)
        {
            Modelos.Usuario u = new Modelos.Usuario();
            u.Name = Convert.ToString(dt.Rows[0]["nombre"]);
            u.Id = Convert.ToInt32(dt.Rows[0]["id"]);
            u.Date = Convert.ToDateTime(dt.Rows[0]["fecha"]);
            u.Email = Convert.ToString(dt.Rows[0]["correo"]);
            u.Password = Convert.ToString(dt.Rows[0]["contrasena"]);
            u.image = Convert.ToString(dt.Rows[0]["image"]);
            u.dinero = Convert.ToDouble(dt.Rows[0]["dinero"]);
            u.nivel = Convert.ToInt32(dt.Rows[0]["nivel"]);
            u.ganadas = Convert.ToInt32(dt.Rows[0]["ganadas"]);
            u.perdidas = Convert.ToInt32(dt.Rows[0]["perdidas"]);
            u.is_google = Convert.ToBoolean(dt.Rows[0]["is_google"]);
            oMain = new FrmMain(u);
            oMain.Show();
            //this.Hide();
        }

        private DataTable Verificar(string email, string contrasena)
        {
            Controladores.Usuario user = new Controladores.Usuario();
            DataTable dt = user.Verificar(email, contrasena);
            return dt;
        }
    }
}
