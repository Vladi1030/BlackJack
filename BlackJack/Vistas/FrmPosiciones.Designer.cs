﻿namespace BlackJack.Vistas
{
    partial class FrmPosiciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtvPosiciones = new System.Windows.Forms.DataGridView();
            this.cNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNivel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnListo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtvPosiciones)).BeginInit();
            this.SuspendLayout();
            // 
            // dtvPosiciones
            // 
            this.dtvPosiciones.AllowUserToAddRows = false;
            this.dtvPosiciones.BackgroundColor = System.Drawing.Color.White;
            this.dtvPosiciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvPosiciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cNum,
            this.cNivel,
            this.cNombre});
            this.dtvPosiciones.Enabled = false;
            this.dtvPosiciones.Location = new System.Drawing.Point(23, 63);
            this.dtvPosiciones.Name = "dtvPosiciones";
            this.dtvPosiciones.ReadOnly = true;
            this.dtvPosiciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtvPosiciones.Size = new System.Drawing.Size(344, 262);
            this.dtvPosiciones.TabIndex = 0;
            // 
            // cNum
            // 
            this.cNum.HeaderText = "Posición";
            this.cNum.Name = "cNum";
            this.cNum.ReadOnly = true;
            // 
            // cNivel
            // 
            this.cNivel.HeaderText = "Nivel";
            this.cNivel.Name = "cNivel";
            this.cNivel.ReadOnly = true;
            // 
            // cNombre
            // 
            this.cNombre.HeaderText = "Nombre";
            this.cNombre.Name = "cNombre";
            this.cNombre.ReadOnly = true;
            // 
            // btnListo
            // 
            this.btnListo.BackColor = System.Drawing.Color.Gray;
            this.btnListo.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListo.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnListo.Location = new System.Drawing.Point(292, 343);
            this.btnListo.Name = "btnListo";
            this.btnListo.Size = new System.Drawing.Size(75, 30);
            this.btnListo.TabIndex = 1;
            this.btnListo.Text = "Listo";
            this.btnListo.UseVisualStyleBackColor = false;
            this.btnListo.Click += new System.EventHandler(this.btnListo_Click);
            // 
            // FrmPosiciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 405);
            this.Controls.Add(this.btnListo);
            this.Controls.Add(this.dtvPosiciones);
            this.Name = "FrmPosiciones";
            this.Text = "Posiciones";
            ((System.ComponentModel.ISupportInitialize)(this.dtvPosiciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtvPosiciones;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNivel;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNombre;
        private System.Windows.Forms.Button btnListo;
    }
}