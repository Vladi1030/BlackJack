﻿using System;
using BlackJack.Utils;

namespace BlackJack.Vistas
{
    public partial class FrmContrasena : MetroFramework.Forms.MetroForm
    {
        private Modelos.Usuario user;

        public FrmContrasena(Modelos.Usuario user)
        {
            InitializeComponent();
            this.user = user;
        }

        private void Cambiar()
        {
            string titulo = "";
            string mensaje = "";
            string password = UserData.Contrasena(this.user.Id);
            if ((!txtNueva.Text.Equals("")) && (!txtNuevaDos.Text.Equals("")))
            {
                if (password.Equals(txtActual.Text.Trim()))
                {
                    if (txtNuevaDos.Text.Trim().Equals(txtNueva.Text.Trim()))
                    {
                        UserData.CambiarContrasena(user.Id, txtNueva.Text.Trim());
                        this.Dispose();
                        return;
                    }
                    else
                    {
                        titulo = "Error";
                        mensaje = "Las contraseñas nuevas no coinciden";
                    }
                }
                else
                {
                    titulo = "Dato Incorrecto";
                    mensaje = "La contraseña actual no es la ingresada";
                }
            }
            else
            {
                titulo = "Error";
                mensaje = "Todos los datos son necesarios";
            }

            FrmMessage oMessage = new FrmMessage(titulo, mensaje);
            oMessage.ShowDialog();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Cambiar();
        }
    }
}
