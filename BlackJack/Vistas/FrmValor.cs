﻿using System;

namespace BlackJack.Vistas
{
    public partial class FrmValor : MetroFramework.Forms.MetroForm
    {
        public int valor { get; set; }
        private string val;

        public FrmValor(string titulo, string message, string valor)
        {
            InitializeComponent();
            this.Text = titulo;
            rtxtMessage.Text = message;
            this.val = valor;
            cargar();
        }

        private void cargar()
        {
            rb1.Checked = true;
            rb1.Visible = true;
            rb11.Visible = true;
        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            valor = 1;
        }

        private void rb11_CheckedChanged(object sender, EventArgs e)
        {
            valor = 11;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}