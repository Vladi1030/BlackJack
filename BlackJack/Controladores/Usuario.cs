﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Controladores
{
    class Usuario : DBAccess.ErrorHandler
    {
        private Modelos.Usuario user;

        public Usuario()
        {
            this.user = new Modelos.Usuario();
        }

        public Usuario(Modelos.Usuario user)
        {
            this.user = user;
        }

        public DataTable Select()
        {
            DataTable result = new DataTable();
            result = this.user.Select();
            if (this.user.isError)
            {
                this.isError = true;
                this.errorDescription = this.user.errorDescription;
            }
            return result;
        }

        public DataTable Verificar(string email, string password)
        {
            DataTable result = new DataTable();
            result = this.user.Verificar(email, password);
            if (this.user.isError)
            {
                this.isError = true;
                this.errorDescription = this.user.errorDescription;
            }
            return result;
        }

        public void Insert()
        {
            this.user.Insert();
            if (this.user.isError)
            {
                this.isError = true;
                this.errorDescription = this.user.errorDescription;
            }
        }

        public void Update(bool ganador, double dinero)
        {
            if (ganador)
            {
                user.UpdateGanadas(dinero);
            }
            else
            {
                user.UpdatePerdidas(dinero);
            }

            if (this.user.isError)
            {
                this.isError = true;
                this.errorDescription = this.user.errorDescription;
            }
        }

        public void UpdateDinero(double dinero)
        {
            this.user.UpdateDinero(dinero);
        }

        public void Delete(int id)
        {
            this.user.Delete();
            if (this.user.isError)
            {
                this.isError = true;
                this.errorDescription = this.user.errorDescription;
            }
        }
    }
}
