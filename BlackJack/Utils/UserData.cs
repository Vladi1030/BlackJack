﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Utils
{
    class UserData
    {
        public static DataTable Posiciones(int nivelMin, int nivelMax)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("nivelMin", nivelMin);
            parametros.Add("nivelMax", nivelMax);
            string query = "SELECT nombre, nivel FROM usuarios WHERE nivel >= @nivelMin and nivel < @nivelMax order by nivel desc;";
            DataTable result = Program.da.SqlQuery(query, parametros);
            return result;
        }

        public static void CambiarContrasena(int id, string password)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("id", id);
            parametros.Add("contrasena", password);
            string query = "UPDATE usuarios SET contrasena = @contrasena WHERE id = @id";
            Program.da.SqlQuery(query, parametros);
        }

        public static string Contrasena(int id)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("id", id);
            string query = "SELECT contrasena FROM usuarios WHERE id = @id";
            DataTable result = Program.da.SqlQuery(query, parametros);
            return Convert.ToString(result.Rows[0]["contrasena"]);
        }
    }
}
