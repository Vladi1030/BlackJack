﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Services
{
    public static class Servicios
    {
        public static string url = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1";
        public static string urlRetrieveCards = "https://deckofcardsapi.com/api/deck/{id}/draw/?count=1";
    }
}
