﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Utils
{
    public class Logica
    {
        public int cuentaJug { get; set; }
        public int cuentaCasa { get; set; }

        public Logica()
        {
        }

        public void aumentarJug(string num)
        {
            this.cuentaJug += value(num);
        }

        public void aumentarCasa(string num)
        {
            escoger(num);
        }

        private void escoger(string val)
        {
            if (val.Equals("ACE"))
            {
                int sum=0;
                if ((cuentaCasa+11) <= 21 || cuentaCasa == 0)
                {
                    sum = 11;
                }
                else
                {
                    sum = 1;
                }
                this.cuentaCasa += value(sum.ToString());
                return;
            }
            this.cuentaCasa += value(val);
        }

        private int value(string val)
        {
            if(val.Equals("KING") || val.Equals("QUEEN") || val.Equals("JACK"))
            {
                return 10;
            }
            else
            {
                return Convert.ToInt32(val);
            }
        }
    }
}