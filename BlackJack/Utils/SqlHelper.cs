﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Utils
{
    class SqlHelper
    {
        private string table;
        public const string[] DEFAULT_WHERE = null;

        public SqlHelper(string table)
        {
            this.table = table;
        }
        public string InsertSql(string[] fields)
        {
            return string.Format("insert into {0} ({1}) values ({2})", this.table, this.Fields(fields), this.ParametrizedFields(fields));
        }

        public string UpdateSql(string[] fields, string[] where = SqlHelper.DEFAULT_WHERE)
        {
            return string.Format("update {0} set {1} {2}", this.table, this.AssignedFields(fields), this.WhereClause(where));
        }

        public string DeleteSql(string[] where = SqlHelper.DEFAULT_WHERE)
        {
            return string.Format("delete from {0} {1}", this.table, this.WhereClause(where));
        }

        public string SelectSql(string[] where = SqlHelper.DEFAULT_WHERE)
        {
            return string.Format("select * from {0} {1}", this.table, this.WhereClause(where));
        }

        private string WhereClause(string[] where)
        {
            if (where == null || where.Length == 0) return string.Empty;
            return string.Format("where {0}", this.AssignedFields(where));
        }

        private string Fields(string[] fields)
        {
            return string.Join(",", fields);
        }

        private string ParametrizedFields(string[] fields)
        {
            return string.Join(",", fields.Select(x => string.Format("@{0}", x)));
        }

        private string AssignedFields(string[] fields)
        {
            return string.Join(",", fields.Select(x => string.Format("{0} = @{1}", x, x)));
        }
    }
}
