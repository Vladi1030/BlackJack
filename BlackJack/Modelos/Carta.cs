﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Modelos
{
    class Carta
    {
        public string image { set; get; }
        public string value { set; get; }
        public string suit { set; get; }
        public string code { set; get; }

        public Carta()
        {

        }
    }
}
