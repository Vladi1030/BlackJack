﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Modelos
{
    public class Deck
    {
        public bool success { get; set; }
        public string deck_id { get; set; }
        public string shuffled { get; set; }
        public int remaining { get; set; }
       
    }
}
