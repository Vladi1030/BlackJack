﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Modelos
{
    class CardsResponse
    {
        public bool success { set; get; }

        public List<Card> cards { set; get; }

        public string deck_id { set; get; }

        public int remaining { set; get; }
        
    }
}
