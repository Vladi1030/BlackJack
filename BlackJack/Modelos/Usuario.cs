﻿using System.Collections.Generic;
using System.Data;
using DBAccess;
using BlackJack.Utils;
using System;

namespace BlackJack.Modelos
{
    public class Usuario : ErrorHandler
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string lastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string image { get; set; }
        public DateTime Date { get; set; }
        public int nivel { get; set; }
        public int ganadas { get; set; }
        public int perdidas { get; set; }
        public double dinero { get; set; }
        public bool is_google { get; set; }

        private SqlHelper sqlHelper;
        private const string TABLE_NAME = "usuario";
        public static readonly string[] TABLE_FIELDS = { "is_google", "nombre", "correo", "fecha", "contrasena", "nivel", "ganadas", "perdidas", "dinero", "image" };

        public Usuario() {
            sqlHelper = new SqlHelper(TABLE_NAME);
        }

        public void Insert()
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("nombre", this.Name);
            parametros.Add("correo", this.Email);
            parametros.Add("fecha", this.Date);
            parametros.Add("contrasena", this.Password);
            parametros.Add("image", this.image);
            parametros.Add("dinero", this.dinero);
            parametros.Add("perdidas", this.perdidas);
            parametros.Add("ganadas", this.ganadas);
            parametros.Add("nivel", this.nivel);
            parametros.Add("is_google", this.is_google);
            this.Id = Convert.ToInt32(Program.da.SqlScalar(this.sqlHelper.InsertSql(TABLE_FIELDS), parametros));
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
            }
        }

        public void Delete()
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("id", this.Id);
            Program.da.SqlStatement(this.sqlHelper.DeleteSql(new string[] { "id" }), parametros);
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
                return;
            }
        }

        public DataTable Select()
        {
            DataTable result = Program.da.SqlQuery(sqlHelper.SelectSql(), new Dictionary<string, object>());
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
            }
            return result;
        }

        public DataTable Verificar(string email, string password)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("correo", email);
            string query = "SELECT * FROM usuario WHERE correo = '" + email + "';";
            DataTable result = Program.da.SqlQuery(query, parametros);
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
            }
            return result;
        }

        public void UpdatePerdidas(double dinero)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("perdidas", this.perdidas+1);
            parametros.Add("dinero", this.dinero-dinero);
            if (this.nivel-19<0)
            {
                nivel = 0;
            }
            else
            {
                nivel -= 19;
            }
            parametros.Add("nivel", nivel);
            Program.da.SqlStatement("UPDATE usuarios SET perdidas = @perdidas, dinero = @dinero, nivel = @nivel WHERE id = id", parametros);
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
                return;
            }
        }

        public void UpdateGanadas(double dinero)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("ganadas", this.ganadas + 1);
            parametros.Add("dinero", this.dinero + dinero);
            parametros.Add("nivel", this.nivel+28);
            parametros.Add("id", this.Id);
            Program.da.SqlStatement("UPDATE usuarios SET ganadas = @ganadas, dinero = @dinero, nivel = @nivel WHERE id = id", parametros);
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
                return;
            }
        }

        public void UpdateDinero(double dinero)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("dinero", this.dinero+dinero);
            parametros.Add("id", this.Id);
            Program.da.SqlStatement("UPDATE usuarios SET dinero = @dinero  WHERE id = id", parametros);
            if (Program.da.isError)
            {
                this.isError = true;
                this.errorDescription = Program.da.errorDescription;
                return;
            }
        }
    }
}
